/*3.8. Un labirint este codificat printr-o matrice de n ×m elemente ale cărui culoare sunt reprezentate prin
elemente egale cu 1, situate în poziţii consecutive pe o aceeasi linie sau coloană, celelalte elemente
fiind 0. O persoană se găseste în poziţia (i, j) din interiorul labirintului. Se cere afisarea tuturor
traseelor de iesire din labirint care nu trec de mai multe ori prin acelasi loc.*/

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

int a[30][30];
int di[4]={-1,0,1,0};
int dj[4]={0,1,0,-1};
int i,j,ns,n,m;

void afisare_mat(int a[30][30])
{
    ns++;
    printf("solutia cu nr %d\n",ns);
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        printf("%d ",a[i][j]);
        printf("\n");
    }

}
void labirint(int a[30][30],int i,int j,int pas)
{
    int k,i1,j1,cond;
    for(k=0;k<=3;k++)
    {

    i1=i+di[k];
    j1=j+dj[k];
    if (i1>=1 && i1<=n && j1>=1 && j1<=m )
    cond=1;
    else
    cond=0;
    if (cond==1)
    {
    if(a[i1][j1]==1)
    {
        a[i1][j1]=pas;
        if(i1==1 || i1==n || j1==1 || j1==m)
        {
            afisare_mat(a);
            if(i1==1 || i1==n){
                if(a[i1][j1-1]==1)
                    {
                    a[i1][j1-1]=pas;
                    afisare_mat(a);
                    a[i1][j1-1]=1;
                    }
                else
                if(a[i1][j1+1]==1)
                    {
                        a[i1][j1+1]=pas;
                        afisare_mat(a);
                        a[i1][j1+1]=1;
                    }
            }
            else
            if(j1==1 || j1==m){
                if(a[i1-1][j1]==1)
                    {
                        a[i-1][j1]=pas;
                        afisare_mat(a);
                        a[i-1][j1]=1;
                    }
                else
                if(a[i1+1][j1]==1)
                    {
                        a[i+1][j1]=pas;
                        afisare_mat(a);
                        a[i+1][j1]=1;
                    }
            }
        }
        else
        labirint(a,i1,j1,pas);
        a[i1][j1]=1;
    }
    }
    }
}
void main()
{
    printf("dati dimensiunile:\n");
    printf("n=");
    scanf("%d",&n);
    printf("m=");
    scanf("%d",&m);
    for(i=1;i<=n;i++)
    for(j=1;j<=m;j++)
    {
        printf("a[%d][%d]=",i,j);
        scanf("%d",&a[i][j]);
    }
    printf("dati pozitia:\n");
    printf("i=");
    scanf("%d",&i);
    printf("j=");
    scanf("%d",&j);
    if(a[i][j]==0)
    printf("nu exista solutii!");
    else
    {
        a[i][j]=-1;
        labirint(a,i,j,-1);

    }


}
