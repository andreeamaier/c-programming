/*cum poti face o suma de bani din mai multe monede=> backtracking*/

#include <stdio.h>
#include <stdlib.h>
#define max 20
int x[max+1],v[max+1],nr[max+1],s,suma;
void afisare(int k)
{
    int i;
    printf("\n");
    for (i=1; i<=k; i++)
    {
        if (x[i]) printf("%d monede de valoarea %d\n",x[i],v[i]);
    }
}
int solutie(int k,int n)
{
    suma=0;
    int i;
    for (i=1; i<=k; i++)
    {
        suma=suma+x[i]*v[i];
        }
    if (suma<=s && k<=n)
            return 1;
    return 0;

}
void back(int k,int n)
{
    int i;
    for (i=0; i<=nr[k];i++)
    {
        x[k]=i;
        if (solutie(k,n)==1)
        {
            if (suma==s)
                afisare(k);
            else
                back(k+1,n);
        }
    }
}

int main()
{   int n;
    printf("Introduceti numerul de monede:");
    scanf("%d",&n);
    printf("Introducet suma dorita!");
    scanf("%d",&s);
    printf("Introduceti valoarea fiecarei monede!");
    int i;
    for (i=1; i<=n; i++)
    {
        scanf("%d",&v[i]);
        nr[i]=s/v[i];
    }
    back(1,n);

    return 0;
}
