/*3.6 Problema turnurilor din Hanoi. Se dau trei tije. Pe una dintre ele sunt asezate n discuri de mărimi
diferite, discurile de diametre mai mici fiind asezate peste discurile cu diametre mai mari. Se cere să
se mute aceste discuri pe o altă tijă, utilizând tija a treia ca intermediar, cu condiţia mutării a câte unui
singur disc si fără a pune un disc de diametru mai mare peste unul cu diametru mai mic.*/

#include <stdio.h>
#include <stdlib.h>

char a,b,c;
int n;

void hanoi (int n, char a, char b, char c)
{
    if (n==1)
        printf("%c -> %c\n",a, b);
    else
    {
        hanoi(n-1,a,c,b);
        printf("%c -> %c\n",a, b);
        hanoi(n-1,c,b,a);
    }
}

int main ( )
{
    printf("Dati numarul de tije=");
    scanf("%d", &n);
    a='a';
    b='b';
    c='c' ;
    hanoi(n,a,b,c);
    return 0;
}
