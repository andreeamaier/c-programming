#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#define NMAX 10
int n;
int nr_solutie;
int x[NMAX];
int verif(int k)
{
    int i;
    for(i = 1; i <= k-1; i++)
        if((x[k] == x[i]) || (abs(k - i) == abs(x[k] - x[i]))
          )
            return 0;
    return 1;
}
void backtr(int k)
{
    int i, j, p;
    for(j = 1; j <= n; j++)
    {
        x[k]=j;
        if(verif(k) == 1)
        {
            if(k < n) backtr(k+1);
            else
            {
                nr_solutie++;
                printf("\nSOLUTIA nr.%d\n", nr_solutie);
                for(i = 1; i <= n; i++)
                {
                    for(p = 1; p <= n; p++)
                        if(x[i] == p) printf("1");
                        else printf("0");
                    printf("\n");
                }
                getch();
            }
        }
    }

}


int main()
{
    printf("\nCe ordin doresti sa aiba tabla? : ");
    scanf("%d",&n);
    int rasp;
    while(n>0)
    {
        if(n>8)
            printf("O tabla de sah poate avea ordinul maxim 8\n");
        else
            if (n>3 && n<=8)
        {
             backtr(1);
        }
        nr_solutie=0;
        fflush(stdin);
        printf("\nDoresti sa aflii solutiile pentru alt ordin? DA=1 / NU=0 \n");
        scanf("%d",&rasp);
        if(rasp==1)
        {
            printf("Ordinul nou: ");
            scanf("%d",&n);
        }
        else exit(1);

    }
    return 0;
}
