/*Fiind date o tabla de sah de dimensiune n � n patrate si un cal plasat �n patratul din st�nga sus
al tablei, sa se afiseze toate posibilitatile de mutare a calului astfel �nc�t sa treaca o singura data prin
fiecare patrat al tablei.*/

#include <stdio.h>
#include <stdlib.h>

int n, nr;
int b[80][80], xi = 0, yi = 0;
int d[][2] = { { -2, -1 }, { -2, 1 }, { -1, 2 }, { 1, 2 }, { 2, 1 }, { 2, -1 }, { 1, -2 }, { -1, -2 } };
//d[x][y] reprezinta miscarile posibile ale calului care se poate deplasa
//doua pozitii pe orizontala/verticala, dupa care una perpendicular pe
//directia pe care s-a deplasat anterior

void Citire()
{
    printf("\n Dati marimea tablei de sah: ");
    scanf("%d", &n);
    printf("\n Dati pozitia de start a calului:");
    printf("\n x=");
    scanf("%d", &xi);
    printf(" y=");
    scanf("%d", &yi);
    xi--;
    yi--;
    b[xi][yi] = 1;
}

int Plin()
{
    int i, j;
    for (i = 0; i<n; i++)
        for (j = 0; j<n; j++)
            if (b[i][j] == 0)
                return 0;
    return 1;
}

void Solutie()
{
    int i, j;
    static int w = 1;
    printf("\n Una dintre solutii este: \n", w++);
    for (i = 0; i<n; i++)
    {
        for (j = 0; j<n; j++)
            printf(" %2d ", b[i][j]);
        printf("\n");
    }

}

int Posibil(int x, int y)
{
    if (x<0 || y<0 || x>n - 1 || y>n - 1)
        return 0;
    if (b[x][y])
        return 0;
    return 1;
}


void Bkt(int x, int y)
{
    static int r = 2;
    int i;

    if (Plin())
    {
        Solutie();

        exit(1);
    }
    for (i = 0; i<8; i++)
        if (Posibil(x + d[i][0], y + d[i][1]))
        {
            b[x + d[i][0]][y + d[i][1]] = r++;
            Bkt(x + d[i][0], y + d[i][1]);
            b[x + d[i][0]][y + d[i][1]] = 0;
            r--;
        }
}

int main()
{

    Citire();
    Bkt(xi, yi);
    printf("\nNu se poate deplasa calul pe toate casutele!!");
    return 0;
}

